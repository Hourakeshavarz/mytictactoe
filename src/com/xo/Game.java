package com.xo;

import javafx.geometry.Pos;

import javax.swing.*;

public class Game {
    Player player1;
    Player player2;
    static Integer turn = 1;
    static GameBoard gameBoard = new GameBoard();
    Game(Player player1, Player player2){
       this.player1=player1;
       this.player2=player2;
    }
    public char run(Integer row, Integer col){
            if(new Round(this, row, col).isValid()){
                System.out.println(turn);
                turn ++;
                gameBoard.getCell(new Position(row, col)).setFilled(true);
                if(turn%2 == 0){
                    gameBoard.getCell(new Position(row,col)).setX(true);
                }else {
                    gameBoard.getCell(new Position(row, col)).setO(true);
                }
                return turn%2==0?'X':'O';
            }
            return 0;
    }
    public  Boolean isGameOver(){
        if(xWon() ||oWon()){
            return true;
        }
        else if(turn==10){
            turn =0;
            return true;
        }
        return false;
    }
    public Boolean xWon(){
        for(int i = 0; i<3;i++){
            if(gameBoard.getCell(new Position(i,0)).getX()==true&&
                    gameBoard.getCell(new Position(i,1)).getX()==true &&
                    gameBoard.getCell(new Position(i,2)).getX()==true){
                return true;
            }
            else if(gameBoard.getCell(new Position(0,i)).getX()==true&&
                    gameBoard.getCell(new Position(1,i)).getX()==true&&
                    gameBoard.getCell(new Position(2,i)).getX()==true){
                return true;
            }
        }
        if(gameBoard.getCell(new Position(0,0)).getX()==true&&
                gameBoard.getCell(new Position(1,1)).getX()==true&&
                gameBoard.getCell(new Position(2,2)).getX()==true){
            return true;
        }else if(gameBoard.getCell(new Position(0,2)).getX()==true&&
                gameBoard.getCell(new Position(1,1)).getX()==true&&
                gameBoard.getCell(new Position(2,0)).getX()==true){
            return true;
        }
        return false;
    }
    public Boolean oWon(){
        for(int i = 0; i<3;i++){
            if(gameBoard.getCell(new Position(i,0)).getO()==true&&
                    gameBoard.getCell(new Position(i,1)).getO()==true &&
                    gameBoard.getCell(new Position(i,2)).getO()==true){
                System.out.println("i,");
                return true;
            }
            else if(gameBoard.getCell(new Position(0,i)).getO()==true&&
                    gameBoard.getCell(new Position(1,i)).getO()==true&&
                    gameBoard.getCell(new Position(2,i)).getO()==true){
                return true;
            }
        }
        if(gameBoard.getCell(new Position(0,0)).getO()==true&&
                gameBoard.getCell(new Position(1,1)).getO()==true&&
                gameBoard.getCell(new Position(2,2)).getO()==true){
            return true;
        }else if(gameBoard.getCell(new Position(0,2)).getO()==true&&
                gameBoard.getCell(new Position(1,1)).getO()==true&&
                gameBoard.getCell(new Position(2,0)).getO()==true){
            return true;
        }
        return false;
    }

    public GameBoard getGameBoard() {
        return gameBoard;
    }

    public static Integer getTurn() {
        return turn;
    }
}
