package com.xo;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class GameBoard {
    private List<Cell> cells = new ArrayList<>();
    public GameBoard() {
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                Cell cell = new Cell(new Position(i, j));
                cells.add(cell);
            }
        }
    }
    public List<Cell> getCells() {
        return cells;
    }
    public Cell getCell(Position position){
        return cells.stream().filter( cell->cell.getPosition().equals(position)).findAny().get();
    }
}
