package com.xo;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import javax.swing.*;

public class Main extends Application {
    Stage primaryStage;
    Game game;
    Pane pane;
    public static void main(String[] args) {
	launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        VBox vbox = new VBox();
        Button classicButton = new Button("Classic");
        classicButton.setStyle("-fx-background-color: #f52887");
        classicButton.setPrefWidth(250);
        classicButton.setPrefHeight(80);

        Button computerButton = new Button("Computer");
        computerButton.setStyle("-fx-background-color: #f52887");
        computerButton.setPrefWidth(250);
        computerButton.setPrefHeight(80);

        Label label = new Label("TicTacToe");
        label.setTextFill(Color.GRAY);
        vbox.getChildren().addAll(label, classicButton, computerButton);

        vbox.setSpacing(50);
        vbox.setStyle("-fx-background-color: #e8adaa");
        vbox.setAlignment(Pos.CENTER);



        primaryStage.setTitle("quoridors");
        Scene scene = new Scene(vbox, 300,300);
        scene.setFill(Color.YELLOW);
        primaryStage.setScene(scene);
        primaryStage.show();
        classicButton.setOnMouseClicked(event ->{
            Scene scene1 = new Scene(nameBox(), 300,300);
            primaryStage.setScene(scene1);
        });
        computerButton.setOnMouseClicked(event -> {
            Scene scene1 =new Scene(new ComputerGame(primaryStage).nameBox(),300,300);
            primaryStage.setScene(scene1);
        });

    }

    public Pane nameBox(){
        VBox vBox = new VBox();
        vBox.setStyle("-fx-background-color: #e8adaa");
        HBox name1 = new HBox();
        HBox name2 = new HBox();
        HBox okbox = new HBox();
        Label player1label = new Label("Player 1: ");
        TextField player1name = new TextField();
        Label player2label = new Label("Player2: ");
        TextField player2name = new TextField();
        Button ok = new Button("Ok");
        ok.setStyle("-fx-background-color: #f52887");
        ok.setPrefWidth(60);
        ok.setPrefHeight(25);
        okbox.getChildren().add(ok);
        okbox.setAlignment(Pos.CENTER);
        name1.getChildren().addAll(player1label, player1name);
        name1.setPadding(new Insets(20));
        name1.setSpacing(30);
        name2.getChildren().addAll(player2label, player2name);
        name2.setPadding(new Insets(20));
        name2.setSpacing(30);
        vBox.getChildren().addAll(name1, name2, okbox);
        ok.setOnMouseClicked(event -> {
            Player player1;
            Player player2;
            if(player1name.getText().length() == 0){
                player1 = new HumanPlayer("Ananymous1")  ;
            }else{
                player1 = new HumanPlayer(player1name.getText());
            }
            if(player2name.getText().length() == 0){
                player2 = new HumanPlayer("Ananymous2")  ;
            }else{
                player2 = new HumanPlayer(player2name.getText());
            }
            game= new Game(player1, player2);
            BorderPane borderPane =new BorderPane();
            borderPane.setStyle("-fx-background-color: #e8adaa");
            borderPane.setTop(new Text(player1.getName()+" X\n"+
                    player2.getName()+" O"));
            borderPane.setCenter(drawBoard(player1, player2));
            Scene scene2 = new Scene(borderPane, 300, 300);
            primaryStage.setScene(scene2);
        });
        return vBox;
    }
    public Pane drawBoard(Player player1, Player player2){
        Rectangle[][] cellsRectangles = new Rectangle[3][3];
        Pane pane =new Pane();
        pane.setStyle("-fx-background-color: #e8adaa");
        for(int j =0; j<3; j++) {
            for (int i = 0; i <3; i++) {
                cellsRectangles[j][i] = new Rectangle(60 * i+65, j*60+65,
                        50, 50);
                cellsRectangles[j][i].setFill(Color.HOTPINK);
                final Integer finalj = j;
                final Integer finali= i;
                pane.getChildren().add(cellsRectangles[j][i]);
                cellsRectangles[j][i].setOnMouseClicked(event -> {
                    Character text = game.run(finalj,finali);
                    if(text!='0'){
                        setLabel(cellsRectangles[finalj][finali],finalj,finali,text);
                        if (game.isGameOver()) {
                            if(game.getTurn()==0){
                                JOptionPane.showMessageDialog(null, "No Winner!");
                            }else {
                                JOptionPane.showMessageDialog(null,
                                        "GameOver!\n" + String.format("Winner is: %s",
                                                game.getTurn() % 2 == 0 ? player1.getName() : player2.getName()));
                            }
                        }
                    };
                });
            }
        }
        return pane;
    }
    public void setLabel(Rectangle rectangle,Integer j, Integer i,Character c){
        pane.getChildren().add(new Text(i*60+85, j*60+85, c.toString()));
   }
}
