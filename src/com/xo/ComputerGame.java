package com.xo;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class ComputerGame {
    Game game;
    Stage stage;
    public ComputerGame(Stage stage) {
        this.stage = stage;
    }
    public Pane nameBox(){
        VBox vBox = new VBox();
        vBox.setStyle("-fx-background-color: #e8adaa");
        HBox name1 = new HBox();
        HBox okbox = new HBox();
        Label player1label = new Label("Player 1: ");
        TextField player1name = new TextField();
        Button ok = new Button("Ok");
        ok.setStyle("-fx-background-color: #f52887");
        ok.setPrefWidth(60);
        ok.setPrefHeight(25);
        okbox.getChildren().add(ok);
        okbox.setAlignment(Pos.CENTER);
        name1.getChildren().addAll(player1label, player1name);
        name1.setPadding(new Insets(20));
        name1.setSpacing(30);
        vBox.getChildren().addAll(name1, okbox);
        ok.setOnMouseClicked(event -> {
            Player player1;
            Player player2;
            if(player1name.getText().length() == 0){
                player1 = new HumanPlayer("Ananymous1")  ;
            }else{
                player1 = new HumanPlayer(player1name.getText());
            }
            player2 = new RandomPlayer("Computer");
            game= new Game(player1, player2 );
            BorderPane borderPane =new BorderPane();
            borderPane.setStyle("-fx-background-color: #e8adaa");
            borderPane.setTop(new Text(player1.getName()+" X\n"+
                    player2.getName()+" O"));
 //           borderPane.setCenter(drawBoard(player1, player2));
            Scene scene2 = new Scene(borderPane, 300, 300);
            stage.setScene(scene2);
        });
        return vBox;
    }

}
