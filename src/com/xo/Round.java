package com.xo;

import javafx.geometry.Pos;

public class Round {
    Game game;
    Position position;
    public Round(Game game,Integer row, Integer col) {
        position = new Position(row, col);
        this.game =game;
    }
    public boolean isValid(){
        if(game.getGameBoard().getCell(position).isFilled==false){
            return true;
        }
        return false;
    }
}
