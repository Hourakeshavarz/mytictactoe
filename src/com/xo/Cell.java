package com.xo;

public class Cell {
    private Position position;
    Boolean isFilled;
    Boolean isX;
    Boolean isO;
    public Cell(Position position) {
        this.position = position;
        isFilled=false;
        isX=false;
        isO=false;
    }

    public Position getPosition() {
        return position;
    }

    public Boolean getFilled() {
        return isFilled;
    }

    public Boolean getX() {
        return isX;
    }

    public Boolean getO() {
        return isO;
    }

    public void setFilled(Boolean filled) {
        isFilled = filled;
    }

    public void setX(Boolean x) {
        isX = x;
    }

    public void setO(Boolean o) {
        isO = o;
    }

    @Override
    public String toString() {
        return "Cell{" +
                "position=" + position +
                ", isFilled=" + isFilled +
                ", isX=" + isX +
                ", isO=" + isO +
                '}';
    }
}
