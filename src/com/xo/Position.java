package com.xo;

public class Position {
    private Integer row;
    private Integer col;

    public Position(int row, int col) {
        this.row = row;
        this.col = col;
    }

    public Integer getRow() {
        return row;
    }

    public Integer getCol() {
        return col;
    }
    public Boolean equals(Position position){
        if(this.getRow()==position.getRow()
        && this.getCol()==position.getCol()){
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "Position{" +
                "row=" + row +
                ", col=" + col +
                '}';
    }
}
